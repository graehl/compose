total 82392
drwxr-xr-x    5 graehl   Administ     4096 Jan 12  2011 z3ta+
-rw-r--r--    1 graehl   Administ     7312 Jun 20  2004 readme.htm
drwxr-xr-x    7 graehl   Administ        0 Dec 28  2010 nubiplus
drwxr-xr-x    5 graehl   Administ        0 Dec 28  2010 nubile
drwxr-xr-x   12 graehl   Administ     4096 Jan  2  2011 lexicon
-rwxr-xr-x    1 graehl   Administ   130560 Sep 17  2009 kxsfi.dll
-rwxr-xr-x    1 graehl   Administ   127488 Sep 17  2009 kXi.dll
drwxr-xr-x    6 graehl   Administ        0 Jan 12  2011 drumatic
-rwxr-xr-x    1 graehl   Administ  3129344 Dec  8  2006 b-station.dll
drwxr-xr-x   14 graehl   Administ     4096 Jan 29  2011 b-station
drwxr-xr-x    4 graehl   Administ        0 Jun 15  2012 Vstplugins
drwxr-xr-x    5 graehl   Administ     4096 Jan 12  2011 Vocal Strip
drwxr-xr-x    1 graehl   Administ        0 Jan 12  2011 VintageChannel
drwxr-xr-x    1 graehl   Administ     4096 Jan 12  2011 Tube Leveler
-rwxr-xr-x    1 graehl   Administ  4077152 Sep 25  2009 TruePianos.dll
-rwxr-xr-x    1 graehl   Administ 12419680 Sep 25  2009 TruePianos x64.dll
drwxr-xr-x    1 graehl   Administ     4096 Jan 12  2011 TransientShaper
-rwxr-xr-x    1 graehl   Administ   688128 Aug 25  2010 Toxic Biohazard.dll
drwxr-xr-x    3 graehl   Administ     4096 Oct 12  2010 The Grand 2
-rwxr-xr-x    1 graehl   Administ  2191360 Nov 14  2009 TAL-BassLine.dll
drwxr-xr-x   20 graehl   Administ     4096 Dec 28  2010 Synth1
-rwxr-xr-x    1 graehl   Administ 14221312 May 10  2005 Symphonic ChoirsVST.dll
drwxr-xr-x    3 graehl   Administ        0 Jan 12  2011 Square I
drwxr-xr-x    1 graehl   Administ     4096 Jan 12  2011 Session Drummer 3
-rwxr-xr-x    1 graehl   Administ   544768 Aug 25  2010 Sawer.dll
-rwxr-xr-x    1 graehl   Administ   540672 Aug 25  2010 Sakura.dll
drwxr-xr-x    1 graehl   Administ        0 Jan 12  2011 SI-String Section
drwxr-xr-x    1 graehl   Administ        0 Jan 12  2011 SI-Electric Piano
drwxr-xr-x    3 graehl   Administ        0 Jan 12  2011 SI-Drum Kit
drwxr-xr-x    1 graehl   Administ        0 Jan 12  2011 SI-Bass Guitar
drwxr-xr-x    6 graehl   Administ        0 Jan 12  2011 SFZ
drwxr-xr-x    3 graehl   Administ        0 Jan 12  2011 Rapture LE
-rwxr-xr-x    1 graehl   Administ   520192 Aug 25  2010 PoiZone.dll
-rwxr-xr-x    1 graehl   Administ  3489598 Oct 25  2010 Pianoteq PLAY.dll
drwxr-xr-x    1 graehl   Administ        0 Oct 11  2010 Pianoteq 2.2
drwxr-xr-x    1 graehl   Administ     4096 Jan 12  2011 Perfect Space
drwxr-xr-x    1 graehl   Administ     4096 Jan 12  2011 Percussion Strip
drwxr-xr-x    6 graehl   Administ     4096 Dec 28  2010 Oatmeal
-rw-r--r--    1 graehl   Administ   102810 Dec 18  2009 MrTramp2.pdf
-rwxr-xr-x    1 graehl   Administ   674816 Feb 28  2010 MrTramp2.dll
-rwxr-xr-x    1 graehl   Administ  5952512 Sep 21  2009 Majken's Chimera.dll
drwxr-xr-x    1 graehl   Administ     8192 Jun 15  2012 Majken's Chimera
-rwxr-xr-x    1 graehl   Administ   140288 Jun 19  2004 MIDIVelocityRangeFilter.dll
-rwxr-xr-x    1 graehl   Administ   135168 Jun 19  2004 MIDIVelocityAmp.dll
-rwxr-xr-x    1 graehl   Administ   137216 Jun 19  2004 MIDINoteTrans.dll
-rwxr-xr-x    1 graehl   Administ   147456 Jun 19  2004 MIDINoteRangeFilter.dll
-rwxr-xr-x    1 graehl   Administ   135168 Jun 19  2004 MIDINoteFilter.dll
-rwxr-xr-x    1 graehl   Administ   160768 Jun 19  2004 MIDIForce2Key.dll
-rwxr-xr-x    1 graehl   Administ   136704 May  5  2004 MIDIChanneliser.dll
-rwxr-xr-x    1 graehl   Administ   137216 May  5  2004 MIDIChannelFilter.dll
-rwxr-xr-x    1 graehl   Administ   151040 Jun 19  2004 MIDICCRangeFilter.dll
drwxr-xr-x    6 graehl   Administ     4096 Jan 12  2011 Linear EQ
drwxr-xr-x    1 graehl   Administ     4096 Jan 12  2011 Linear Compressor
-rwxr-xr-x    1 graehl   Administ 31894856 Jul 15  2010 Kontakt 4.dll
-rwxr-xr-x    1 graehl   Administ   297472 Jul 15  2010 Kontakt 4 8out.dll
-rwxr-xr-x    1 graehl   Administ   297472 Jul 15  2010 Kontakt 4 16out.dll
-rwxr-xr-x    1 graehl   Administ  4907008 Sep 20  2007 Ivory VST.dll
-rwxr-xr-x    1 graehl   Administ  6373376 Jan 20  2006 INTRO.dll
drwxr-xr-x    7 graehl   Administ     4096 Jan  1  2011 INTRO
-rwxr-xr-x    1 graehl   Administ   450560 May 15  2010 IL Drumpad.dll
-rwxr-xr-x    1 graehl   Administ   532480 Aug 25  2010 IL Drumaxx.dll
-rwxr-xr-x    1 graehl   Administ   520192 Aug 25  2010 Hardcore.dll
-rwxr-xr-x    1 graehl   Administ  7190528 Sep 19  2009 Grizzly.dll
drwxr-xr-x   33 graehl   Administ     8192 Jan  1  2011 Grizzly
-rwxr-xr-x    1 graehl   Administ   929280 Jan 12  2011 FL Studio VSTi.dll
-rwxr-xr-x    1 graehl   Administ   929280 Jan 12  2011 FL Studio VSTi (Multi).dll
drwxr-xr-x    3 graehl   Administ        0 Jan 30  2011 E-MU
drwxr-xr-x    7 graehl   Administ     4096 Jan 12  2011 DropZone
-rw-r--r--    1 graehl   Administ    31332 May 23  2010 DVS Saxophone.pdf
-rwxr-xr-x    1 graehl   Administ  1072640 May 23  2010 DVS Saxophone.dll
drwxr-xr-x    1 graehl   Administ        0 Jan  1  2011 DVS Saxophone
drwxr-xr-x    1 graehl   Administ     4096 Jan 12  2011 Channel Tools
drwxr-xr-x    6 graehl   Administ     4096 Jan 12  2011 Cakewalk Sound Center
drwxr-xr-x    5 graehl   Administ        0 Jan 12  2011 Boost11
drwxr-xr-x    4 graehl   Administ        0 Jan 12  2011 BitMeter
-rw-r--r--    1 graehl   Administ     6148 Feb 19  2004 2EDS_~!3
-rwxr-xr-x    1 graehl   Administ 15863808 Aug 13  2003 04EWPercVST.dll
-rwxr-xr-x    1 graehl   Administ 15863808 Aug 13  2003 03EWBrassVST.dll
-rwxr-xr-x    1 graehl   Administ 15863808 Aug 13  2003 02EWWoodwindsVST.dll
-rwxr-xr-x    1 graehl   Administ 15515648 Aug 13  2003 01EWStringsVST.dll
